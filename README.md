# DMTools
This application is meant to provide the core tools used by a dungeon master to run a D&D session. It includes a monster database searcher, an encounter builder, a dice roller, and a simple notes section.

## Overview
### Monster Database Searcher
The monster database searcher allows you to search an SQLite database with a pre-set database. The database used for this application was generated from [this JSON file](https://gist.github.com/tkfu/9819e4ac6d529e225e9fc58b358c3479). The JSON file consists of only monsters in the SRD.  

You can add monsters from this searcher to the current encounter by right-clicking on the monster in question. If you want to add 5 of a particular monster, hold shift before right-clicking. To add an arbitrary number of monsters to the encounter, hold control before right-clicking, and a popup will appear asking for the number of monsters you wish to add. 

### Encounter Builder/Manager
The encounter builder and manager allows the DM to add a series of Monsters and Players. The feature also allows the user to track whose turn it is, initiative, HP, and time elapsed in combat (time elapsed in combat in measured as 6 seconds for every 10 turns). Furthermore, you can save and load preset encounters for later use.

You can remove monsters from the encounter by right clicking on them.

### Dice Roller
The dice rolling section allows the DM to add a series of dice with an arbitrary number of sides. They can be rolled independently, or rolled all at once.

You can remove dice from the dice pool by right-clicking on them.

### Notes
The notes section is just a simple text field, very similar to notepad on Windows, that a DM can use to jot down quick notes.

### Misc.
Whenever the DM closes the software, it will save all previous session data to be re-used later. Upon re-opening the software, it will load in the previous session data to resume your previous adventure!

## Building/Running

In order to build this application yourself, you need to have [Maven](https://maven.apache.org/) installed. After you have maven installed and ready to use, running the software is extremely easy, simply execute:

```
mvn clean javafx:run
```

And to package the app into a .jar file use


```
mvn clean install
```

NOTE: This app uses the maven shade plugin to create an uber jar, which includes all dependencies.

## Downloading/Installing

If you do not care to modify the code, or build it yourself, you can use a pre-built image. Pre-built images are located on the [downloads](https://bitbucket.org/ajackson2681/dmtools/downloads/) page of this repo. You can download either the [latest](https://bitbucket.org/ajackson2681/dmtools/downloads/dmtools-latest.zip) image, or select another version. Simply download, unzip and run the .jar file.