package dev.adamjackson.dmtools;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AddMonstersPopup extends VBox {
    private Stage parentStage;
    private int numMonstersToAdd = 0;
    private boolean shouldEmitFlag = false;
    @FXML TextField input;

    @FXML private void initialize() {
        // saves a reference to "this" because "this" becomes different inside
        // the anonymous class for the ChangeListener
        AddMonstersPopup ref = this;

        // limits inputs to integers
        ref.input.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, 
				String newValue) {
				if (!newValue.matches("\\d*")) {
					ref.input.setText(newValue.replaceAll("[^\\d]", ""));
				}
				
				if (!ref.input.getText().equals("")) {
					ref.numMonstersToAdd = Integer.parseInt(ref.input.getText());
				}
			}
		});
    }

    /**
     * Closes the window without allowing the values to be retrieved
     * 
     * @param e
     */
    @FXML private void close(ActionEvent e) {
        this.parentStage.close();
    }

    /**
     * Closes the window and allowing the values to be retrieved
     * @param e
     */
    @FXML private void confirm(ActionEvent e) {
        this.shouldEmitFlag = true;
        this.parentStage.close();
    }

    public void setStage(Stage stage) {
        this.parentStage = stage;
    }

    public int getNumMonsters() {
        return this.numMonstersToAdd;
    }

    public boolean valuesShouldBeEmitted() {
        return this.shouldEmitFlag;
    }
}