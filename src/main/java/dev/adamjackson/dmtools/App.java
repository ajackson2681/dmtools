package dev.adamjackson.dmtools;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

/**
 * JavaFX App
 */
public class App extends Application {
	
    public static String VERSION = "DEV";
    public static String dbURL;
    public static Scene scene;
    public static Stage mainStage;
    public static boolean maximized;

    @Override
    public void start(Stage stage) throws IOException, FileNotFoundException {

        this.loadVersionString();

        mainStage = stage;
        
        App.dbURL = loadDBLocation();

    	// show error if unable to connect to the database, then close
    	if (!SQLInterface.connect()) {
			Alert a = new Alert(AlertType.WARNING);
			a.setTitle("SQLite DB Error");
			a.setHeaderText("Unable to connect to the database file.");
			a.showAndWait();
			System.exit(1);
    	}

        scene = new Scene(loadFXML("PrimaryController"));
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("Dungon Master Tools v"+VERSION);
        stage.show();

        WindowManager.initStageDimensions();
    }

    @Override
    public void stop() {
        try {
            JSONParser.saveAllData();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private void loadVersionString() {

        try {
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                    App.class.getResourceAsStream("VERSION")
                )   
            );
            App.VERSION = reader.readLine();
        }
        catch (IOException | NullPointerException ex ) {
            // don't do anything, file doesn't exist
        }
    }

    private String loadDBLocation() throws FileNotFoundException, IOException {
        String url = "";
        String workingDirectory = App.getWorkingDirectory().getAbsolutePath();

        if (new File(workingDirectory+"/dblocation.txt").exists()) {
            FileReader fr = new FileReader(workingDirectory+"/dblocation.txt");

            int data;
            while ((data = fr.read()) != -1) {
                url += (char)data;
            }

            fr.close();
        }
        else {
            url = workingDirectory+"/monsters.db.sqlite";
            FileWriter fw = new FileWriter(workingDirectory+"/dblocation.txt");
            fw.write(url);
            fw.close();
        }
        
        return url;
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(
            App.class.getResource(fxml + ".fxml")
        );
        return fxmlLoader.load();
    }
    
    public static FXMLLoader getLoader(String fxml) {
        return new FXMLLoader(App.class.getResource(fxml + ".fxml"));
    }

    /**
     * Returns a File reference to the parent directory of the application's 
     * .jar file. You can get the file path string from this by calling
     * .getAbsolutePath() on the File returned from this function.
     *  
     * @return File of .jar's parent dir
     */
    public static File getApplicationDirectory() {
        File retVal = null;

        try {
            retVal = new File(App.class.getProtectionDomain()
                                       .getCodeSource()
                                       .getLocation()
                                       .toURI()).getParentFile(); 
        }
        catch (URISyntaxException ignore) {
            // Won't happen because the application path _has_ to exist for the
            // application to be running.
        }

        return retVal;
    }

    /**
     * The application can be launched from a different directory than where the
     * .jar is located. This gets the directory from which the application was
     * launched and returns a File reference to it.
     * 
     * @return The File reference of where the application was launched from. 
     */
    public static File getWorkingDirectory() {
        return new File(System.getProperty("user.dir"));
    }

    public static void launchWrapper(String[] args) {
    	launch(args);
    }
}