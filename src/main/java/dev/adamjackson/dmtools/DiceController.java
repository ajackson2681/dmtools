package dev.adamjackson.dmtools;

import java.io.IOException;
import java.util.Collection;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

public class DiceController {

    @FXML private ListView<DiceEntry> diceList;
    @FXML private TextArea diceRollOutput;

	/**
	 * Adds a new die to the diceList
	 */
	@FXML private void addDie() throws IOException {		
		this.diceList.getItems().add(new DiceEntry(this.diceRollOutput));
	}
	
	/**
	 * Removes selected die from the die list. Does nothing if no die
	 * is selected.
	 */
	@FXML private void removeDie() {
		DiceEntry toRemove = this.diceList.getSelectionModel().getSelectedItem();

		if (toRemove != null) {
			this.diceList.getItems().remove(toRemove);
		}

		this.diceList.getSelectionModel().clearSelection();
	}
	
	public void roll(DiceEntry e) {
		this.diceRollOutput.setText("");
		
		int total = 0;
		
		for (int i = 0; i < e.getNumDie(); i++) {
			int roll = RNG.roll(e.getDieValue());
			total += roll;
			this.diceRollOutput.appendText("Roll "+(i+1)+" for d"+e.getDieValue()+" = "+roll+"\n");
		}
		
		this.diceRollOutput.appendText("Total roll for d"+e.getDieValue()+" = "+total+"\n");
	}

	/**
	 * Rolls a separate die for each value equal to the number of dice for that particular value,
	 * and then also totals them. The information for this is displayed in the diceRollOutput TextArea.
	 * Example with output:
	 * 
	 * <pre>
	 * 1 d6
	 * 4 d4
	 * 
	 * Roll 1 for d6 = 3
	 * Total roll for d6 = 3
	 * Roll 1 for d4 = 3
	 * Roll 2 for d4 = 1
	 * Roll 3 for d4 = 4
	 * Roll 4 for d4 = 3
	 * Total roll for d4 = 11
	 * </pre>
	 */
	@FXML private void rollAll() {
		this.diceRollOutput.setText("");
		
		for (DiceEntry die : this.diceList.getItems()) {
			if (die.getNumDie() == 0 || die.getDieValue() == 0) {
				continue;
			}
			int numOfDie = die.getNumDie();
			int dieValue = die.getDieValue();
		
			int total = 0;
			
			for (int i = 0; i < numOfDie; i++) {
				int roll = RNG.roll(dieValue);
				total += roll;
				this.diceRollOutput.appendText("Roll "+(i+1)+" for d"+dieValue+
					" = "+roll+"\n");
			}
			
			this.diceRollOutput.appendText("Total roll for d"+dieValue+" = "+
				total+"\n");
			
		}
	}
	
	/**
	 * Clears the text in the results area on a whim. This will happen whenever 
	 * a new set of dice are rolled, but this allows the user to clear the 
	 * TextArea whenever. Currently cleas the area on a context menu click 
	 * (right click by default).
	 */
	@FXML private void clearText() {
		this.diceRollOutput.setText("");
	}

	@SuppressWarnings("incomplete-switch")
	@FXML private void onDiceListClick(MouseEvent e) {
		switch (e.getButton()) {
			case SECONDARY:
				this.removeDie();
				break;
		}
	}

	public void add(DiceEntry die) {
		this.diceList.getItems().add(die);
	}

	public void addAll(Collection<DiceEntry> diceList) {
		this.diceList.getItems().addAll(diceList);
	}

	public ObservableList<DiceEntry> getDiceList() {
		return this.diceList.getItems();
	}

}
