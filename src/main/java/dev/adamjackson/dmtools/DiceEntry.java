package dev.adamjackson.dmtools;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class DiceEntry extends HBox {

	@FXML private TextField numDieInput;	
	@FXML private TextField dieValueInput;

	private int numDie = 0;
	private int dieValue = 0;
	
	public DiceEntry() {
		try {		
			FXMLLoader loader = App.getLoader("DiceEntry");
			loader.setController(this);
			loader.setRoot(this);
			loader.load();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public DiceEntry(TextArea resultArea) {
		this();
		this.numDie = 0;
		this.dieValue = 0;		
		this.afterInit();
	}
	
	public DiceEntry(int numDie, int dieValue) {
		this();
		this.numDie = numDie;
		this.dieValue = dieValue;
		this.afterInit();
	}


	private void afterInit() {
		this.numDieInput.setText(String.valueOf(this.numDie));
		this.dieValueInput.setText(String.valueOf(this.dieValue));
	}

	/**
	 * Sets the number of the die selection (i.e. how many of a particular die there is) on
	 * each keypress. This removes the need to confirm your selection with enter or tab
	 */
	@FXML private void numDieInput() {
		try {
			this.numDie = Integer.parseInt(numDieInput.getText());
		}
		catch (NumberFormatException ex) {
			this.numDieInput.setText("");
		}
	}
	
	/**
	 * Sets the value of the die selection (i.e. the max possible roll of the current die) on
	 * each keypress. This removes the need to confirm your selection with enter or tab
	 */
	@FXML private void dieValueInput() {
		try {
			this.dieValue = Integer.parseInt(dieValueInput.getText());
		}
		catch (NumberFormatException ex) {
			this.dieValueInput.setText("");
		}
	}
	
	@FXML private void roll() {
		PrimaryController.diceController.roll(this);
	}
	
	public int getNumDie() {
		return this.numDie;
	}
	
	public int getDieValue() {
		return this.dieValue;
	}

	
}

