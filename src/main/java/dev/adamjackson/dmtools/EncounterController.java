package dev.adamjackson.dmtools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import com.google.gson.JsonElement;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.FileChooser.ExtensionFilter;

public class EncounterController {
	@FXML private ListView<EncounterEntry> encounterList;
	@FXML private Label timeElapsedLabel;
	@FXML private Button startEncounterButton;
	@FXML private Button stopEncounterButton;
	@FXML private Button nextTurnButton;
	private int numTurnsTaken = 0;
	private int turnIndex = 0;

	public void sortEncounterList() {
		this.encounterList.getItems().sort(new Comparator<EncounterEntry>() {
			@Override
			public int compare(EncounterEntry o1, EncounterEntry o2) {
				return o2.getInitiative() - o1.getInitiative();
			}
		});
	}
	
	@FXML private void removeFromEncounter() {
		EncounterEntry curSelection = this.encounterList.getSelectionModel().getSelectedItem();
		
		if (curSelection != null) {
			this.encounterList.getItems().remove(curSelection);
		}

		this.encounterList.getSelectionModel().clearSelection();
	}

	@FXML private void addPlayer() throws IOException {
		Stage popup = new Stage();    
		popup.setResizable(false);    
        popup.initModality(Modality.APPLICATION_MODAL);
		popup.initStyle(StageStyle.UNDECORATED);
        
		FXMLLoader loader = App.getLoader("PlayerAddPopup");
		Scene scene = new Scene(loader.load());

		popup.setTitle("Add New Player");
		popup.setScene(scene);
		popup.show();
		popup.setOnHidden(e -> {
			PlayerAddPopup popupRef = loader.getController();
			
			if (popupRef.valuesShouldBeEmitted()) {
				PlayerData data = popupRef.getPlayerData();

				EncounterMetadata em = new EncounterMetadata(data);
				EncounterEntry toAdd = new EncounterEntry(em);
				this.encounterList.getItems().add(toAdd);
			}
		});
	}
	
	@SuppressWarnings("incomplete-switch")
	@FXML public void encounterListOnClick(MouseEvent e) {
		switch(e.getButton()) {
			case SECONDARY:
				this.removeFromEncounter();
				break;
		}
	}
	
	@FXML private void startEncounter() {
		if (this.encounterList.getItems().size() == 0) {
			return;
		}
		
		this.turnIndex = 0;
		this.numTurnsTaken = 0; 
		
		for (EncounterEntry entry : this.encounterList.getItems()) {
			entry.unSetActiveTurn();
		}
		
		this.encounterList.getItems().get(this.turnIndex).setActiveTurn();

		this.startEncounterButton.setDisable(true);
		this.stopEncounterButton.setDisable(false);
		this.nextTurnButton.setDisable(false);
	}
	
	@FXML private void endEncounter() {
		if (this.encounterList.getItems().size() == 0) {
			return;
		}
		this.turnIndex = 0;
		this.numTurnsTaken = 0;

		for (EncounterEntry entry : this.encounterList.getItems()) {
			entry.unSetActiveTurn();
		}
		
		this.timeElapsedLabel.setText("Time Elapsed: 0 second(s)");

		this.startEncounterButton.setDisable(false);
		this.stopEncounterButton.setDisable(true);
		this.nextTurnButton.setDisable(true);
	}

	@FXML private void nextTurn() {
		if (this.encounterList.getItems().size() == 0) {
			return;
		}
		
		this.encounterList.getItems().get(this.turnIndex++).unSetActiveTurn();
		this.numTurnsTaken++;

		if (this.turnIndex == this.encounterList.getItems().size()) {
			this.turnIndex = 0;
		}

		int secondsPassed = 6*((int)this.numTurnsTaken/10);
		this.timeElapsedLabel.setText("Time Elapsed: "+secondsPassed+" second(s)");
		
		this.encounterList.getItems().get(this.turnIndex).setActiveTurn();
	}

	@FXML private void saveEncounter() {
		FileChooser fc = new FileChooser();
		fc.setInitialDirectory(App.getWorkingDirectory());
		fc.setTitle("Save Encounter");
		fc.getExtensionFilters().addAll(new ExtensionFilter("JSON", "*.json"));

		File toSave = fc.showSaveDialog(App.mainStage);
		if (toSave != null) {
			JSONParser.saveEncounter(this.encounterList.getItems(), 
										toSave.getAbsolutePath());
		}
	}

	@FXML private void loadEncounter() {
		FileChooser fc = new FileChooser();
		fc.setInitialDirectory(App.getWorkingDirectory());
		fc.setTitle("Load Encounter");
		fc.getExtensionFilters().addAll(new ExtensionFilter("JSON", "*.json"));

		File toLoad = fc.showOpenDialog(App.mainStage);
		if (toLoad != null) {
			JsonElement encounterJSON = 
				JSONParser.loadJSONFile(toLoad.getAbsolutePath());
			if (encounterJSON != null) {
				ArrayList<EncounterEntry> encounterList = 
					JSONParser.loadEncounter(encounterJSON.getAsJsonArray());
				this.encounterList.getItems().addAll(encounterList);
			}
		}
	}

	public void addToEncounter(EncounterEntry entry) {
		this.encounterList.getItems().add(entry);
	}

	public void addToEncounter(Collection<EncounterEntry> entries) {
		this.encounterList.getItems().addAll(entries);
	}

	public ObservableList<EncounterEntry> getEncounterList() {
		return this.encounterList.getItems();
	}
}
