package dev.adamjackson.dmtools;

import java.io.IOException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class EncounterEntry extends HBox {

	private EncounterMetadata metadata;
	private boolean selected;
	
	@FXML TextField initiativeInput;
	@FXML TextField hpInput;
	@FXML Label activeIndicator;
	@FXML Label nameLabel;
	@FXML Label acLabel;
	@FXML Label maxHpLabel;
	@FXML Button hpMinus;
	@FXML Button hpPlus;
	
	public EncounterEntry(EncounterMetadata encounterMetadata) {
		this.metadata = encounterMetadata;

		try {
			FXMLLoader loader = App.getLoader("EncounterEntry");
			loader.setController(this);
			loader.setRoot(this);
			loader.load();
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}

		// create ref here since inside of the change listener 'this' changes
		EncounterEntry ref = this;

		ref.initiativeInput.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, 
				String newValue) {
				if (!newValue.matches("\\d*")) {
					ref.initiativeInput.setText(newValue.replaceAll("[^\\d]", ""));
				}
				
				if (!ref.initiativeInput.getText().equals("")) {
					int initiative = Integer.parseInt(ref.initiativeInput.getText());
					ref.setInitiative(initiative);
					PrimaryController.encounterController.sortEncounterList();
				}
			}
		});

		ref.hpInput.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, 
				String newValue) {
				if (!newValue.matches("\\d*")) {
					ref.hpInput.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}

	@FXML public void initialize() {
		if (this.metadata != null) {
			this.hpInput.setText(String.valueOf(this.metadata.getCurrentHp()));
			this.maxHpLabel.setText("/ "+this.metadata.getMaxHp());
			this.acLabel.setText(String.valueOf(this.metadata.getArmorClass()));
			this.nameLabel.setText(this.metadata.getName());
		}

		this.activeIndicator.setText("");
	}
	
	@FXML public void decrementHp() {
		int curVal = Integer.parseInt(this.hpInput.getText());
		
		curVal--;
		
		if (curVal < 0) {
			curVal = 0;
		}
		
		this.hpInput.setText(String.valueOf(curVal));
	}
	
	@FXML public void incrementHp() {
		int curVal = Integer.parseInt(this.hpInput.getText());
		
		curVal++;
		
		this.hpInput.setText(String.valueOf(curVal));
	}

	public int getInitiative() {
		return this.metadata.getInitiative();
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public boolean getSelected() {
		return this.selected;
	}
	
	public void setActiveTurn() {
		this.activeIndicator.setText("<-");
	}

	public void unSetActiveTurn() {
		this.activeIndicator.setText("");
	}
	
	/**
	 * This is really only used when adding a player as an encounter entry, since we use the 
	 * "currentHP" field from the PlayerData class to set this. Monsters just assume that their
	 * HP is full at the beginning of the encounter.
	 * @param value
	 */
	public void setCurrentHP(String value) {
		this.hpInput.setText(value);
	}

	public EncounterMetadata getMetadata() {
		return this.metadata;
	}

	public void setInitiative(int initiative) {
		this.metadata.setInitiative(initiative);
		this.initiativeInput.setText(String.valueOf(this.metadata.getInitiative()));
	}

	public void setCurHp(int curHp) {
		this.hpInput.setText(String.valueOf(curHp));
	}

	public int getCurHp() {
		try {
			return Integer.parseInt(this.hpInput.getText());
		}
		catch(NumberFormatException ex) {
			return 0;
		}
	}
}
