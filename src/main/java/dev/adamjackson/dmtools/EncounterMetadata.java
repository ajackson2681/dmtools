package dev.adamjackson.dmtools;

public class EncounterMetadata {
    private String name;
    private String armorClass;
    private int currentHp;
    private int maxHp;
    private int initiative;

    public EncounterMetadata(Monster monster) {
        this.name = monster.getName();
        this.maxHp = monster.getCombatHP();
        this.currentHp = this.maxHp;
        this.armorClass = monster.getArmorClass();   
    }

    public EncounterMetadata(PlayerData player) {
        this.name = player.name;
        this.maxHp = player.maxHp;
        this.currentHp = player.curHp;
        this.armorClass = String.valueOf(player.ac);
    }

    public int getCurrentHp() {
        return this.currentHp;
    }

    public void setCurrentHp(int currentHp) {
        this.currentHp = currentHp;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(String armorClass) {
        this.armorClass = armorClass;
    }

    public int getInitiative() {
        return initiative;
    }

    public void setInitiative(int initiative) {
        this.initiative = initiative;
    }
}
