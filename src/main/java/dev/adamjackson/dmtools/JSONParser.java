package dev.adamjackson.dmtools;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javafx.collections.ObservableList;

public class JSONParser {

    public static void saveAllData() {
        ObservableList<EncounterEntry> encounterList = 
            PrimaryController.encounterController.getEncounterList();
        ObservableList<DiceEntry> dieList =
            PrimaryController.diceController.getDiceList();
        ObservableList<NotesEntry> notesList =
            PrimaryController.notesController.getContent();
            
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonObject obj = new JsonObject();

        JsonArray arr = new JsonArray();
        JsonObject toAdd = null;
        for (EncounterEntry ee : encounterList) {
            toAdd = gson.toJsonTree(ee.getMetadata()).getAsJsonObject();
            arr.add(toAdd);
        }
        obj.add("encounter_list", arr);
        
        arr = new JsonArray();
        for (DiceEntry d : dieList) {
            JsonObject dieObj = new JsonObject();
            
            // We create the die objects in JSON like this since we can't serialize JavaFX 
            // components. But we really only care about these two values anyway.
            dieObj.addProperty("numDie", d.getNumDie());
            dieObj.addProperty("dieValue", d.getDieValue());
            arr.add(dieObj);
        }
        obj.add("dice_list", arr);

        arr = new JsonArray();
        for (NotesEntry e : notesList) {
            JsonObject notesObj = new JsonObject();

            notesObj.addProperty("noteHeader", e.getHeader());
            notesObj.addProperty("noteConent", e.getNotes());
            arr.add(notesObj);
        }
        obj.add("notes_list", arr);

        // flush the data out to disk
        try {
            String appPath = App.getApplicationDirectory().getAbsolutePath();
            FileWriter fw = new FileWriter(appPath+"/save_data.json");
            fw.write(gson.toJson(obj));
            fw.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void saveEncounter(ObservableList<EncounterEntry> encounterList,
        String filePath) 
    {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonArray arr = new JsonArray();
        JsonObject toAdd = null;
        for (EncounterEntry ee : encounterList) {
            toAdd = gson.toJsonTree(ee.getMetadata()).getAsJsonObject();
            arr.add(toAdd);
        }

        // flush the data out to disk
        try {
            FileWriter fw = new FileWriter(filePath);
            fw.write(gson.toJson(arr));
            fw.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static JsonElement loadJSONFile(String filePath) {
        try {
            Gson gson = new Gson(); // reader only
            Reader reader = Files.newBufferedReader(Paths.get(filePath));
            JsonElement obj = gson.fromJson(reader, JsonElement.class);
            return obj;
        }
        catch (IOException e) {
            return null;
        }
    }

    /**
     * Accepts a JsonArray and parses each element to generate a list of 
     * EncounterEntry objects.
     * @return
     */
    public static ArrayList<EncounterEntry> loadEncounter(JsonArray encounterList) {
        if (encounterList == null) {
            return null;
        }

        try {
            // reference to return. This will be added to the ListView
            ArrayList<EncounterEntry> encounterEntryList = 
                new ArrayList<EncounterEntry>();

            Gson gson = new Gson(); // reader only
            
            for (JsonElement entry : encounterList) {
                // have to cast to avoid errors
                JsonObject encounter = entry.getAsJsonObject();
                EncounterMetadata em = 
                    gson.fromJson(encounter, EncounterMetadata.class);

                EncounterEntry toAdd = new EncounterEntry(em);
                toAdd.setInitiative(encounter.get("initiative").getAsInt());
                toAdd.setCurHp(encounter.get("currentHp").getAsInt());
                encounterEntryList.add(toAdd);
            }

            return encounterEntryList;
        }
        catch (Exception ex) {
            // return null on exceptions so we know to ignore any data from this
            // function.
            return null;
        }
    }

    /**
     * Accepts a JsonArray and parses each element to generate a list of 
     * DiceEntry objects.
     * @param diceList json array to parse for DiceEntry data
     * @return
     */
    public static ArrayList<DiceEntry> getDiceList(JsonArray diceList) {
        if (diceList == null) {
            return null;
        }

        try {
            ArrayList<DiceEntry> diceEntryList = new ArrayList<DiceEntry>();

            for (JsonElement entry : diceList) {
                JsonObject die = entry.getAsJsonObject();
                int numDie = die.get("numDie").getAsInt();
                int dieValue = die.get("dieValue").getAsInt();
                diceEntryList.add(new DiceEntry(numDie, dieValue));
            }

            return diceEntryList;
        }
        catch (Exception e) {
            return null;
        }
    }

    public static ArrayList<NotesEntry> loadNotes(JsonArray notesList) {
        if (notesList == null) {
            return null;
        }

        try {
            ArrayList<NotesEntry> notesEntryList = new ArrayList<NotesEntry>();

            for (JsonElement entry : notesList) {
                JsonObject note = entry.getAsJsonObject();
                String label = note.get("noteHeader").getAsString();
                String content = note.get("noteConent").getAsString();
                notesEntryList.add(new NotesEntry(label, content));
            }

            return notesEntryList;
        }
        catch (Exception e) {
            return null;
        }
    }
}
