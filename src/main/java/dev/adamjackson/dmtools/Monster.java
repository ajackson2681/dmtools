package dev.adamjackson.dmtools;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Monster {
	private static class Schema {
		public static final String NAME = "name";
		public static final String METADATA = "meta";
		public static final String ARMOR_CLASS = "ArmorClass";
		public static final String HITPOINTS = "HitPoints";
		public static final String MOVEMENT_SPEEDS = "Speed";
		public static final String STRENGTH = "str";
		public static final String DEXTERITY = "dex";
		public static final String CONSTITUTION = "con";
		public static final String INTELLIGENCE = "int";
		public static final String WISDOM = "wis";
		public static final String CHARISMA = "cha";
		public static final String SAVING_THROWS = "SavingThrows";
		public static final String SKILLS = "skills";
		public static final String DAMAGE_RESISTANCES = "DamageResistances";
		public static final String DAMAGE_IMMUNITIES = "DamageImmunities";
		public static final String CONDITION_IMMUNITIES = "ConditionImmunities";
		public static final String SENSES = "Senses";
		public static final String LANGUAGES = "Languages";
		public static final String CHALLENGE_RATING = "Challenge";
		public static final String TRAITS = "Traits";
		public static final String ACTIONS = "Actions";
		public static final String LEGENDARY_ACTIONS = "LegendaryActions";
		public static final String HIT_DIE = "HitDie";
		public static final String HIT_DIE_MODIFIER = "HitDieModifier";
	}

	private String name = "name";
	private String metadata = "meta";
	private String armorClass = "ArmorClass";
	private String hitpoints = "HitPoints";
	private String movementSpeeds = "Speed";
	private String strength = "str";
	private String dexterity = "dex";
	private String constitution = "con";
	private String intelligence = "int";
	private String wisdom = "wis";
	private String charisma = "cha";
	private String savingThrows = "SavingThrows";
	private String skills = "skills";
	private String damageResistances = "DamageResistances";
	private String damageImmunities = "DamageImmunities";
	private String conditionImmunities = "ConditionImmunities";
	private String senses = "Senses";
	private String languages = "Languages";
	private String challengeRating = "Challenge";
	private String traits = "Traits";
	private String actions = "Actions";
	private String legendaryActions = "LegendaryActions";
	private String hitDie = "HitDie";
	private String hitDieModifier = "HitDieModifier";

	/**
	 * Builds a monster object out of the ResultSet (tuple) that we get from the Monster DB
	 * @param rs is the tuple to create a monster out of
	 * @throws SQLException
	 */
	public Monster(ResultSet rs) throws SQLException {
		this.name = rs.getString(Schema.NAME);
		this.metadata = rs.getString(Schema.METADATA);
		this.armorClass = rs.getString(Schema.ARMOR_CLASS);
		this.hitpoints = rs.getString(Schema.HITPOINTS);
		this.movementSpeeds = rs.getString(Schema.MOVEMENT_SPEEDS);
		this.strength = rs.getString(Schema.STRENGTH);
		this.dexterity = rs.getString(Schema.DEXTERITY);
		this.constitution = rs.getString(Schema.CONSTITUTION);
		this.intelligence = rs.getString(Schema.INTELLIGENCE);
		this.wisdom = rs.getString(Schema.WISDOM);
		this.charisma = rs.getString(Schema.CHARISMA);
		this.savingThrows = rs.getString(Schema.SAVING_THROWS);
		this.skills = rs.getString(Schema.SKILLS);
		this.damageResistances = rs.getString(Schema.DAMAGE_RESISTANCES);
		this.damageImmunities = rs.getString(Schema.DAMAGE_IMMUNITIES);
		this.conditionImmunities = rs.getString(Schema.CONDITION_IMMUNITIES);
		this.senses = rs.getString(Schema.SENSES);
		this.languages = rs.getString(Schema.LANGUAGES);
		this.challengeRating = rs.getString(Schema.CHALLENGE_RATING);
		this.traits = rs.getString(Schema.TRAITS);
		this.actions = rs.getString(Schema.ACTIONS);
		this.legendaryActions = rs.getString(Schema.LEGENDARY_ACTIONS);
		this.hitDie = rs.getString(Schema.HIT_DIE);
		this.hitDieModifier = rs.getString(Schema.HIT_DIE_MODIFIER);
	}
	
	public String getMetadata() {
		return metadata;
	}

	public String getArmorClass() {
		return this.armorClass;
	}

	public String getHitpoints() {
		return hitpoints;
	}

	public String getMovementSpeeds() {
		return movementSpeeds;
	}

	public String getStrength() {
		return strength;
	}

	public String getDexterity() {
		return dexterity;
	}

	public String getConstitution() {
		return constitution;
	}

	public String getIntelligence() {
		return intelligence;
	}

	public String getWisdom() {
		return wisdom;
	}

	public String getCharisma() {
		return charisma;
	}

	public String getSavingThrows() {
		return savingThrows;
	}

	public String getSkills() {
		return skills;
	}

	public String getDamageResistances() {
		return damageResistances;
	}

	public String getDamageImmunities() {
		return damageImmunities;
	}

	public String getConditionImmunities() {
		return conditionImmunities;
	}

	public String getSenses() {
		return senses;
	}

	public String getLanguages() {
		return languages;
	}

	public String getChallengeRating() {
		return challengeRating;
	}

	public String getTraits() {
		return traits;
	}

	public String getActions() {
		return actions;
	}

	public String getLegendaryActions() {
		return legendaryActions;
	}

	public String getHitDie() {
		return hitDie;
	}

	public String getHitDieModifier() {
		return hitDieModifier;
	}

	public String getName() {
		return this.name;
	}
	
	/**
	 * generates a random hp value using the hit die and modifier. This creates
	 * more uniqueness among creatures.
	 * @return
	 */
	public int getCombatHP() {
		String[] dieList = this.hitDie.split(("d"));
		int numDie = Integer.parseInt(dieList[0]);
		int numFacesPerDie = Integer.parseInt(dieList[1]);
		
		int modifier = Integer.parseInt(this.hitDieModifier);

		int generatedHp = 0;

		for (int i = 0; i < numDie; i++) {
			generatedHp += RNG.roll(numFacesPerDie);
		}

		generatedHp += modifier;

		return generatedHp;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
