package dev.adamjackson.dmtools;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MonsterDBController extends VBox {

    /**
	 * This is all the stuff related to the db browser
	 */
	@FXML private TextField searchBar;
	@FXML private ListView<Monster> monsterList;
	
	@FXML public void searchDB() {
		this.monsterList.getItems().clear();
		SQLInterface.query(searchBar.getText());
		this.monsterList.getItems().addAll(SQLInterface.lastQuery);
	}
	
	/**
	 * Adds the currently selected creature to the encounter.
	 */
	private void addToEncounter(MouseEvent e) {
		Monster currentSelection = this.monsterList.getSelectionModel().getSelectedItem();
		if (currentSelection == null) {
			return;
		}

		if (e.isShiftDown()) {
			this.addToEncounter(5, currentSelection);
		}
		else if (e.isControlDown()) { 
			try {
				Stage popup = new Stage();    
				popup.setResizable(false);    
				popup.initModality(Modality.APPLICATION_MODAL);
				popup.initStyle(StageStyle.UNDECORATED);
				
				FXMLLoader loader = App.getLoader("AddMonstersPopup");
				Scene scene = new Scene(loader.load());
				AddMonstersPopup addMonstersPopup = 
					loader.getController();
				addMonstersPopup.setStage(popup);
		
				popup.setAlwaysOnTop(true);
				popup.setScene(scene);
				popup.show();
				
				popup.setOnHidden( e1 -> {
					if (addMonstersPopup.valuesShouldBeEmitted()) {
						int numberToAdd = addMonstersPopup.getNumMonsters();
						this.addToEncounter(numberToAdd, currentSelection);
					}
				});
			}
			catch (IOException ignore) {}
		}
		else {
			this.addToEncounter(1, currentSelection);
		}
	}

	/**
	 * Displays the stat block of the currently selected monster in a popup just to the right of
	 * the monster list view. It can be closed by un-focusing on the popup.
	 */
	private void displayStatBlock() {

		Monster currentSelection = this.monsterList.getSelectionModel().getSelectedItem();
		
		if (currentSelection != null) {
			PrimaryController.monsterDataController.populateStatBlock(currentSelection);
		}
	}
	
	/**
	 * When a monster is selected, we want to display a pop-up with their stat block. This popup
	 * can be closed by clicking off of it, or selecting a different monster.
	 */
	@SuppressWarnings("incomplete-switch")
	@FXML public void onClick(MouseEvent e) {
		switch (e.getButton()) {
			case PRIMARY:
				this.displayStatBlock();
				break;
			case SECONDARY:
				this.addToEncounter(e);
				break;
		}
	}   

	/**
	 * Adds a specified number of the current selection of monsters to the 
	 * encounter.
	 * 
	 * @param numberToAdd
	 * @param currentSelection
	 */
	private void addToEncounter(int numberToAdd, Monster currentSelection) {
		for (int i = 0; i < numberToAdd; i++) {
			EncounterMetadata em = new EncounterMetadata(currentSelection);
			PrimaryController.encounterController.addToEncounter(
				new EncounterEntry(em)
			);
		}
	}
}
