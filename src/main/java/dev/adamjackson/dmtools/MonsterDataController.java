package dev.adamjackson.dmtools;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

public class MonsterDataController {
	
	@FXML private VBox infoContainer;
	@FXML private Label headerLabel;
	@FXML private Label strValue;
	@FXML private Label dexValue;
	@FXML private Label conValue;
	@FXML private Label intValue;
	@FXML private Label wisValue;
	@FXML private Label chaValue;
	@FXML private Label hpValue;
	@FXML private Label acValue;
	@FXML private Label spdValue;
	@FXML private Label crValue;
	@FXML private Label skillsValue;
	@FXML private Label sensesValue;
	@FXML private Label languagesValue;
	@FXML private Label additionalValue;
	@FXML private Label actionsValue;
	@FXML private WebView abilities;

	@FXML private void initialize() {
		this.infoContainer.setVisible(false);
	}

	/**
	 * Sets all initial values for the stat block card
	 */
	public void populateStatBlock(Monster monster) {
		if (monster != null) {
			this.strValue.setText(monster.getStrength());
			this.dexValue.setText(monster.getDexterity());
			this.conValue.setText(monster.getConstitution());
			this.intValue.setText(monster.getIntelligence());
			this.wisValue.setText(monster.getWisdom());
			this.chaValue.setText(monster.getCharisma());
			this.acValue.setText(monster.getArmorClass());
			this.hpValue.setText(monster.getHitpoints());
			this.spdValue.setText(monster.getMovementSpeeds());
			this.crValue.setText(monster.getChallengeRating());
			this.skillsValue.setText(monster.getSkills());
			this.sensesValue.setText(monster.getSenses());
			this.languagesValue.setText(monster.getLanguages());
			this.additionalValue.setText(monster.getMetadata());
			this.abilities.getEngine().loadContent(monster.getActions());
			this.headerLabel.setText("Stat Block - "+monster.getName());
			this.abilities.getEngine().setUserStyleSheetLocation(
				getClass().getResource("WebView.css").toString()
			);
			this.infoContainer.setVisible(true);
		}
	}

	public void hideStatblock() {
		this.infoContainer.setVisible(false);
	}
}
