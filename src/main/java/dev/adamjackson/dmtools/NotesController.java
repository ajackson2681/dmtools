package dev.adamjackson.dmtools;

import java.io.IOException;
import java.util.ArrayList;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class NotesController {

    @FXML private ListView<NotesEntry> notesList;
    @FXML private VBox mainBox;
    @FXML private AnchorPane notesContainer;
    @FXML private TextField newNoteInput;
    private NotesView notesView;

    @FXML public void initialize() throws IOException {
        FXMLLoader loader = App.getLoader("NotesView");
        Parent component = loader.load();

        // make the notes view stretch to the full size o fthe anchor pane
        AnchorPane.setBottomAnchor(component, 0.0);
        AnchorPane.setLeftAnchor(component, 0.0);
        AnchorPane.setRightAnchor(component, 0.0);
        AnchorPane.setTopAnchor(component, 0.0);
        this.notesContainer.getChildren().add(component);

        // accessing the notesview controller
        this.notesView = loader.getController();

        // binds the managed property to the visible property, so that when the
        // visibility gets set to false, the component also gets unmanaged in
        // the view (i.e. it won't take up space in the view).
        this.notesContainer.managedProperty().bind(
            this.notesContainer.visibleProperty()
        );
        this.notesList.managedProperty().bind(
            this.notesList.visibleProperty()
        );
        this.newNoteInput.managedProperty().bind(
            this.newNoteInput.visibleProperty()
        );

        this.notesContainer.setVisible(false);
        this.notesList.setVisible(true);
    }

    @FXML private void addEntry() {
        String header = "New note";

        if (!this.newNoteInput.getText().equals("")) {
            header = this.newNoteInput.getText();
        }

        this.notesList.getItems().add(new NotesEntry(header, ""));

        this.newNoteInput.setText("");
    }

    @FXML private void deleteSelected() {
        NotesEntry curSelection = this.notesList.getSelectionModel()
                                                .getSelectedItem();

        if (curSelection != null) {
            this.notesList.getItems().remove(curSelection);
            this.notesList.getSelectionModel().clearSelection();
        }
    }

    // to be used in PrimaryController to add a loaded list from disk
    public void addAll(ArrayList<NotesEntry> toAdd) {
        this.notesList.getItems().addAll(toAdd);
    }

    public void setActiveNote(NotesEntry entry) {
        this.notesList.setVisible(false);
        this.newNoteInput.setVisible(false);
        this.notesContainer.setVisible(true);
        this.notesView.setActiveNote(entry);
    }

    public void collapseEntry() {
        this.notesList.setVisible(true);
        this.newNoteInput.setVisible(true);
        this.notesContainer.setVisible(false);
        this.notesView.setActiveNote(null);
    }

    public void deleteNote(NotesEntry entry) {
        this.notesList.getItems().remove(entry);
        this.collapseEntry();
    }
    
    public ObservableList<NotesEntry> getContent() {
        return this.notesList.getItems();
    }
}
