package dev.adamjackson.dmtools;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class NotesEntry extends HBox {

	@FXML private Label headerLabel;

	private String header;
	private String notes;

    public NotesEntry(String header, String notes) {
        try {		
			FXMLLoader loader = App.getLoader("NotesEntry");
			loader.setController(this);
			loader.setRoot(this);
			loader.load();
			this.header = header;
			this.notes = notes;
			this.headerLabel.setText(header);
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
    } 

	public void edit() {
		PrimaryController.notesController.setActiveNote(this);
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setHeader(String header) {
		this.header = header;
		this.headerLabel.setText(header);
	}

	public String getHeader() {
		return this.header;
	}
}
