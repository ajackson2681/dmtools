package dev.adamjackson.dmtools;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.ContextMenuEvent;

public class NotesView {
    @FXML private TextField notesHeader;
    @FXML private TextArea notesArea;
    @FXML private Button saveButton;
    private NotesEntry entryRef;

    public void setActiveNote(NotesEntry entry) {
        this.entryRef = entry;

        if (entry != null) {
            this.notesArea.setText(entry.getNotes());
            this.notesHeader.setText(entry.getHeader());
        }
        else {
            this.notesArea.setText("");
            this.notesHeader.setText("");
        }

        this.saveButton.setDisable(true);
    }

    @FXML private void collapse() {
        PrimaryController.notesController.collapseEntry();
    }

    @FXML public void saveNote() {
        this.entryRef.setNotes(this.notesArea.getText());
        this.entryRef.setHeader(this.notesHeader.getText());
        this.saveButton.setDisable(true);
    }

    @FXML public void deleteNote() {
        PrimaryController.notesController.deleteNote(
            this.entryRef
        );
    }

    @FXML private void markNoteChanged() {
        this.saveButton.setDisable(false);
    }

    @FXML private void consume(ContextMenuEvent e) {
        e.consume();
    }
}
