package dev.adamjackson.dmtools;

import java.util.HashMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class PlayerAddPopup {
    @FXML TextField playerNameInput;
    @FXML TextField playerMaxHPInput;
    @FXML TextField playerACInput;
    @FXML TextField playerCurHPInput;

    private HashMap<String, String> preValues = new HashMap<>();
    private boolean emitValuesFlag;

    @FXML public void initialize() {
        this.preValues.put("playerMaxHPInput", this.playerMaxHPInput.getText());
        this.preValues.put("playerACInput", this.playerACInput.getText());
        this.preValues.put("playerCurHPInput", this.playerCurHPInput.getText());
    }

    @FXML private void onButtonPress(KeyEvent e) {
        TextField src = (TextField)e.getSource();

        switch(src.getId()) {
            case "playerMaxHPInput":
                this.preValues.put("playerMaxHPInput", this.playerMaxHPInput.getText());
                break;
            case "playerACInput":
                this.preValues.put("playerACInput", this.playerACInput.getText());
                break;
            case "playerCurHPInput":
                this.preValues.put("playerCurHPInput", this.playerCurHPInput.getText());
                break;
        }
    }

    @FXML private void onButtonReleased(KeyEvent e) {
        TextField src = (TextField)e.getSource();

        switch(src.getId()) {
            case "playerMaxHPInput":
                try {
                    Integer.parseInt(this.playerMaxHPInput.getText());
                }
                catch(NumberFormatException ex) {
                    this.playerMaxHPInput.setText(this.preValues.get("playerMaxHPInput"));
                }
                break;
            case "playerACInput":
                try {
                    Integer.parseInt(this.playerACInput.getText());
                }
                catch(NumberFormatException ex) {
                    this.playerACInput.setText(this.preValues.get("playerACInput"));
                }
                break;
            case "playerCurHPInput":
                try {
                    Integer.parseInt(this.playerCurHPInput.getText());
                }
                catch(NumberFormatException ex) {
                    this.playerCurHPInput.setText(this.preValues.get("playerCurHPInput"));
                }
                break;
        }
    }

    @FXML private void submit(ActionEvent e) {
        boolean invalidInput = false;

        if (this.playerACInput.getText().compareTo("") == 0) {
            invalidInput = true;
        }

        if (this.playerCurHPInput.getText().compareTo("") == 0) {
            invalidInput = true;
        }

        if (this.playerMaxHPInput.getText().compareTo("") == 0) {
            invalidInput = true;
        }

        if (this.playerNameInput.getText().compareTo("") == 0) {
            invalidInput = true;
        }

        if (!invalidInput) {
            this.emitValuesFlag = true;
            Node  source = (Node)  e.getSource(); 
            Stage stage  = (Stage) source.getScene().getWindow();
            stage.close();
        }
        else {
			Alert a = new Alert(AlertType.WARNING);
			a.setTitle("Player Input Error");
			a.setHeaderText("Player input is invalid. One or more fields are empty.");
			a.showAndWait();    
        }
    }

    @FXML private void cancel(ActionEvent e) {
        Node  source = (Node)  e.getSource(); 
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }
    
    public boolean valuesShouldBeEmitted() {
        return this.emitValuesFlag;
    }

    public PlayerData getPlayerData() {
        PlayerData retVal = new PlayerData();
        try {
            retVal.ac = Integer.parseInt(this.playerACInput.getText());
            retVal.curHp = Integer.parseInt(this.playerCurHPInput.getText());
            retVal.maxHp = Integer.parseInt(this.playerMaxHPInput.getText());
            retVal.name = this.playerNameInput.getText();
            
            return retVal;
        }
        catch (NumberFormatException ex) {
            return null;
        }
    }
}
