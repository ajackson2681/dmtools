package dev.adamjackson.dmtools;

/**
 * This is just a class that is meant to act as a struct to hold data to be returned from the 
 * PlayerAddPopup.
 */
public class PlayerData {
    public int curHp;
    public int maxHp;
    public int ac;
    public String name;

    public boolean allFieldsValid() {
        return this.curHp > 0 && this.maxHp > 0 && this.ac > 0 && this.name.compareTo("") == 0;
    }
}
