package dev.adamjackson.dmtools;

import java.io.IOException;
import java.util.ArrayList;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.transform.Scale;

public class PrimaryController {
	@FXML private GridPane layoutGrid;
	@FXML private Label titleLabel;
	@FXML private VBox mainContainer;
	@FXML private Group rootGroup;

	// these are public accessors so we don't have to pass them as references
	// to the child controllers.
	public static MonsterDBController monsterDBController;
	public static DiceController diceController;
	public static EncounterController encounterController;
	public static NotesController notesController;
	public static MonsterDataController monsterDataController;

	@FXML public void initialize() throws IOException {

		FXMLLoader loader = App.getLoader("MonsterDBController");
		Parent component = loader.load();
		this.layoutGrid.add(component, 0, 0);
		PrimaryController.monsterDBController = loader.getController();
		
		loader = App.getLoader("DiceController");
		component = loader.load();
		PrimaryController.diceController = loader.getController();
		this.layoutGrid.add(component, 0, 1);
		
		loader = App.getLoader("EncounterController");
		component = loader.load();
		this.layoutGrid.add(component, 1, 0, 1, 2);
		PrimaryController.encounterController = loader.getController();

		loader = App.getLoader("NotesController");
		component = loader.load();
		this.layoutGrid.add(component, 2, 0);
		PrimaryController.notesController = loader.getController();

		loader = App.getLoader("MonsterDataController");
		component = loader.load();
		this.layoutGrid.add(component, 2, 1);
		PrimaryController.monsterDataController = loader.getController();

		String appPath = App.getApplicationDirectory().getAbsolutePath();
		JsonElement savedData = 
			JSONParser.loadJSONFile(appPath+"/save_data.json");

		if (savedData != null) {
			this.parseSavedData(savedData.getAsJsonObject());
		}	

		this.titleLabel.setText("DMTools v"+App.VERSION);
	}
	
	@FXML private void closeApplication() {
		Platform.exit();
	}

	@FXML private void minimize() {
		WindowManager.minimize();
	}

	@FXML private void toggleMaximized() {
		WindowManager.toggleMaximized();
	}

	@FXML private void setInitialMousePosition(MouseEvent e) {
		WindowManager.initMousePos(e);
	}
	
	@FXML private void dragEffect(MouseEvent e) {
		WindowManager.drag(e);
	}

	@FXML private void endDrag(MouseEvent e) {
		WindowManager.endDrag(e);
	}

	@FXML private void mouseClick(MouseEvent e) {
		if (e.getButton() == MouseButton.PRIMARY) {
			if (e.getClickCount() == 2) {
				WindowManager.toggleMaximized();
			}
		}
	}

	@FXML private void resize(MouseEvent e) {
		WindowManager.resize(e);
	}

	
	@FXML private void checkResizable(MouseEvent e) {
		WindowManager.checkResizable(e);
	}

	@FXML private void stopResizing() {
		WindowManager.stopResizing();
	}

	private void parseSavedData(JsonObject savedData) {
		// load the data in on startup. Only fill in non-null values
		JsonElement notesJSON = savedData.get("notes_list");
		if (notesJSON != null) {
			ArrayList<NotesEntry> notesList = 
				JSONParser.loadNotes(notesJSON.getAsJsonArray());
			PrimaryController.notesController.addAll(notesList);
		}

		JsonElement encounterListJSON = savedData.get("encounter_list");
		if (encounterListJSON != null) {
			ArrayList<EncounterEntry> encounterList = 
				JSONParser.loadEncounter(encounterListJSON.getAsJsonArray());
			PrimaryController.encounterController.addToEncounter(encounterList);
		}

		JsonElement diceListJSON = savedData.get("dice_list");
		if (diceListJSON != null) {
			ArrayList<DiceEntry> diceList = 
				JSONParser.getDiceList(diceListJSON.getAsJsonArray());
			PrimaryController.diceController.addAll(diceList);
		}
	}

	public void scaleUI(double scaleFactor) {
		Scale scale = new Scale(scaleFactor, scaleFactor);
        scale.setPivotX(0);
        scale.setPivotY(0);       
        this.rootGroup.getTransforms().setAll(scale);
	}
}
