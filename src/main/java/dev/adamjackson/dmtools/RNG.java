package dev.adamjackson.dmtools;

import java.nio.charset.Charset;
import java.util.Random;
import oshi.SystemInfo;

public class RNG {
	
	private static Random generator;
	
	private static Random createGenerator() {
		long currentTime = System.currentTimeMillis();
		long freeRam = new SystemInfo().getHardware().getMemory().getAvailable();
		long heapSize = Runtime.getRuntime().totalMemory();
    	return new Random(currentTime * freeRam * heapSize);
	}
	
	public static int roll(int max) {
		if (RNG.generator == null) {
			// this ensures that the heap won't be predictable in size, as it won't get
			// created until the first call to roll
			RNG.generator = RNG.createGenerator();
		}

		return (1 + RNG.generator.nextInt(max));
	}

	public static String createRandomString(int size) {
		if (RNG.generator == null) {
			// this ensures that the heap won't be predictable in size, as it won't get
			// created until the first call to roll
			RNG.generator = RNG.createGenerator();
		}

		byte[] array = new byte[size]; // length is bounded by 7
		new Random().nextBytes(array);
		return new String(array, Charset.forName("UTF-8"));
	}
}
