package dev.adamjackson.dmtools;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This is a class of static methods to communicate with the monster database so that the 
 * complicated stuff can be abstracted away.
 * @author Adam Jackson
 *
 */
public class SQLInterface {
	
	public static final ObservableList<Monster> lastQuery = FXCollections.observableArrayList();
	
	private static Connection conn;
	
	public static boolean connect() {
    	try {
    		// only load if the file exists, if it does not, don't try to create it.
    		if (new File(App.dbURL).exists()) {
	    		Class.forName("org.sqlite.JDBC");
	        	SQLInterface.conn = DriverManager.getConnection("jdbc:sqlite:"+App.dbURL);
	        	return true;
    		}
    	}
    	catch (ClassNotFoundException | SQLException ex) {
    		ex.printStackTrace();
    	}
    	
    	return false;
	}
	
	/**
	 * Returns the schema of the database (i.e. the column headers of the monster table).
	 * @return
	 */
	public static String getSchema() {
		String schema = "";
		
		if (SQLInterface.conn != null) {
			try {
	    		Statement stmt = SQLInterface.conn.createStatement();
	    		String query = "select * from monsters";
	    		ResultSet rs = stmt.executeQuery(query);
	    		ResultSetMetaData rsmd = rs.getMetaData();
	    		
	    		for (int i = 1; i < rsmd.getColumnCount(); i++) {
	    			if (i != rsmd.getColumnCount()) {
	    				schema += rsmd.getColumnName(i)+" | ";
	    			}
	    			else {
	    				schema += rsmd.getColumnName(i);
	    			}
	    		}
			}
			catch (SQLException ex) {
				ex.printStackTrace();
				return null;
			}
		}
		
		return schema;
	}
	
	/**
	 * Queries the SQL database for the Monster. Bases all searches on the name column of the DB.
	 * @param name is the name of the creature(s) you are searching for.
	 */
	public static void query(String name) {
		SQLInterface.lastQuery.clear();
		
		if (SQLInterface.conn != null) {
			try {
	    		Statement stmt = SQLInterface.conn.createStatement();
	    		String query = "select * from monsters where name like \"%"+name+"%\"";

	    		ResultSet rs = stmt.executeQuery(query);
	    		
	    		while (rs.next()) {
	    			Monster m = new Monster(rs);
	    			
	    			SQLInterface.lastQuery.add(m);
	    		}
			}
			catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
}
