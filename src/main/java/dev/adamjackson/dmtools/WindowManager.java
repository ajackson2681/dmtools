package dev.adamjackson.dmtools;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Popup;
import javafx.stage.Screen;

/**
 * This class handles all of the math/logic for window resizing and movement
 * operations. This way, the details can be abstracted away from the controller
 * classes, and they can be kept cleaner.
 */
public class WindowManager {

    // utility classes/enums for data encapsulation
    private static enum SnapDirection {
        LEFT,
        RIGHT
    }

    private static class Dimensions {
        double width = 0;
        double height = 0;
    }

    private static class Point {
        double x = 0;
        double y = 0;
    }

    private static boolean maximized = false;

    // this determines if the window is snapped to the left or right of the 
    // screen. This is needed because the logic to resize on dragging the 
    // window is different than when the window is maximized or regular.
    private static boolean snappedToSide = false;

    // this flag is used to check if the window was being dragged when the mouse
    // is released
    private static boolean dragging = false;

    // the origin is the top left corner of the window
    private static Point origin = new Point();

    // this is used in calculating the mouse move diff so we know how much to
    // move the origin by
    private static Point prevMouse = new Point();
    private static Point prevMouseResize = new Point();

    // the dimensions of the window when minimized
    private static Dimensions dimensions = new Dimensions();

    // this reference will tell us when we need to/don't need to draw the 
    // outline
    private static Popup snapOutline = null;

    private static boolean resizing = false;
    /**
     * This function is called every time the mouse is pressed down on the
     * title bar. This sets the initial position of the mouse so that all 
     * future calculations can be done relative to that initial point.
     * 
     * @param e MouseEvent from when the mouse was clicked down
     */
    public static void initMousePos(MouseEvent e) {
        prevMouse.x = e.getScreenX();
		prevMouse.y = e.getScreenY();
    }

    /**
     * This is called every time a drag event occurs. This is wrapped in
     * PrimaryController. This calculates a diff in both the x & y directions,
     * based on the mouse's current position, and the position from the previous
     * event. This diff is then applied to the "origin" which is the top-left
     * corner of the application window. The coordinates from this event are
     * then saved for the next call.
     * 
     * @param e Mouse Event for the current drag event
     */
    public static void drag(MouseEvent e) {

        if (resizing) {
            return;
        }

        if (maximized) {
            // center the window on the mouse based on the minimized dimensions
            origin.x = e.getScreenX() - dimensions.width/2;
            origin.y = e.getScreenY() - e.getSceneY();
        }
        else if (snappedToSide) {
            // center the window on the mouse based on the current dimensions
            origin.x = e.getScreenX() - App.mainStage.getWidth()/2;
            origin.y = e.getScreenY() - e.getSceneY();
        }

        // when dragging, always toggle maximized and snapped off
		maximized = false;
        snappedToSide = false;
        dragging = true;

        // calculate the diff from the last event and the current event
		double diffX = prevMouse.x - e.getScreenX();
		double diffY = prevMouse.y - e.getScreenY();

        // apply the diffs to the origin point
        origin.x -= diffX;
        origin.y -= diffY;

        // assign the previous mouse coordinaes to the current event
		prevMouse.x = e.getScreenX();
		prevMouse.y = e.getScreenY();

		adjustWindow();
		checkBounds(e);
    }

    /**
     * Hides the snap outline popup
     */
    public static void hideSnapOutline() {
        if (snapOutline != null) {
            snapOutline.hide();
            snapOutline = null;
        }
    }

    /**
     * This event actually fires any time the mouse is released. Because of this
     * we need to check if the window was being dragged.
     * @param e
     */
    public static void endDrag(MouseEvent e) {
        hideSnapOutline();

        if (dragging) {
            checkForExpansion(e);
        }

        dragging = false;
    }

    /**
     * Maximizes window if it's not, unmaximizes it if it is.
     */
    public static void toggleMaximized() {
		if (!maximized) { 
			App.mainStage.setX(getMinX());
			App.mainStage.setY(getMinY());
			App.mainStage.setWidth(getWidth());
			App.mainStage.setHeight(getHeight());
		}
		else {
			App.mainStage.setX(origin.x);
			App.mainStage.setY(origin.y);
			App.mainStage.setWidth(dimensions.width);
			App.mainStage.setHeight(dimensions.height);
		}

		maximized = !maximized;
        snappedToSide = false;
    }

    /**
     * Minimizes window down to the tray. Java calls this "iconified".
     */
    public static void minimize() {
        App.mainStage.setIconified(true);
    }

    /**
     * Displays a partially transparent boundary indicating how the window will
     * fill the screen upon releasing the mouse.
     * 
     * @param origin top left corner of the boundary region
     * @param size height and width of the boundary region
     * @throws IOException if the SnapPreview fxml file is not present
     */
    private static void displaySnapBoundaries(Point origin, 
                                              Dimensions size) 
                                              throws IOException 
    {
        // create a new popup and make it mostly transparent so we can still
        // see the main window through it
        snapOutline = new Popup();
        snapOutline.setOpacity(0.35);
        
        // get the UI component for displaying the "snap" boundaries
        FXMLLoader loader = App.getLoader("SnapPreview");
        Pane comp = loader.load();

        // set the size of the popup
        snapOutline.setX(origin.x);
        snapOutline.setY(origin.y);
        snapOutline.setWidth(size.width);
        snapOutline.setHeight(size.height);

        // have to set this in code since USE_COMPUTED_STYLE doesn't seem to 
        // work properly. If it isn't set to explicity be the same size as its
        // parent, it won't stretch to fill
        comp.setPrefHeight(snapOutline.getHeight());
        comp.setPrefWidth(snapOutline.getWidth());

        // finally after resizing the component, add it to the popup and display
        // it
        snapOutline.getContent().add(comp);
        snapOutline.show(App.mainStage, origin.x, origin.y);
    }

    /**
     * Checks if the mouse has reached one of the boundaries, and whether or not
     * to display the "snap preview"
     * 
     * @param e MouseEvent passed from drag()
     */
    private static void checkBounds(MouseEvent e) {
        boolean showSnapOutline = false;

        Point snapOrigin = new Point();
        Dimensions snapDimensions = new Dimensions();

        // top of screen
        if (e.getScreenY() <= 0) {
            showSnapOutline = true;
            snapOrigin.x = getMinX();
            snapOrigin.y = getMinY();
            snapDimensions.width = getWidth();
            snapDimensions.height = getHeight();
        }   
        else if (e.getScreenX() <= 0) { // left side    
            showSnapOutline = true;
            snapOrigin.x = getMinX();
            snapOrigin.y = getMinY();
            snapDimensions.width = getWidth()/2;
            snapDimensions.height = getHeight();
        }
        else if (e.getScreenX() >= getWidth() - 1) { // right side   
            showSnapOutline = true;
            snapOrigin.x = getWidth()/2;
            snapOrigin.y = getMinY();
            snapDimensions.width = getWidth()/2;
            snapDimensions.height = getHeight();
        }

        if (!showSnapOutline) { 
            hideSnapOutline();
        }
        else if (snapOutline == null && showSnapOutline) {
            try {
                displaySnapBoundaries(snapOrigin, snapDimensions);
            }
            catch (IOException ignore) {}
        }
    }

    /**
     * This is just called once at the application startup to set the initial
     * values.
     */
	public static void initStageDimensions() {
        dimensions.height = App.mainStage.getHeight();
        dimensions.width = App.mainStage.getWidth();
        origin.x = App.mainStage.getX();
        origin.y = App.mainStage.getY();

        App.mainStage.widthProperty().addListener((obs, oldVal, newVal) -> {
            App.mainStage.getScene().getRoot().prefWidth(App.mainStage.getWidth());
            App.mainStage.getScene().getRoot().minWidth(App.mainStage.getWidth());
            App.mainStage.getScene().getRoot().maxWidth(App.mainStage.getWidth());
        });

        App.mainStage.heightProperty().addListener((obs, oldVal, newVal) -> {
            App.mainStage.getScene().getRoot().prefHeight(App.mainStage.getHeight());
            App.mainStage.getScene().getRoot().minHeight(App.mainStage.getHeight());
            App.mainStage.getScene().getRoot().maxHeight(App.mainStage.getHeight());
        });
	}

    /**
     * Resizes the window along the borders. Lets the user stretch the window
     * to a desired size.
     * 
     * @param e MouseEvent used to determine the direction in which to stretch
     * the window.
     */
    public static void resize(MouseEvent e) {
        Scene scene = App.mainStage.getScene();

        if (scene.getCursor() == Cursor.N_RESIZE) {
            double diff = e.getScreenY() - prevMouseResize.y;

            dimensions.height -= diff;
            origin.y += diff;
            
            prevMouseResize.y = e.getScreenY();
        }
        else if (scene.getCursor() == Cursor.E_RESIZE) {
            double diff = e.getScreenX() - prevMouseResize.x;
            
            dimensions.width += diff;

            prevMouseResize.x = e.getScreenX();
        }
        else if (scene.getCursor() == Cursor.S_RESIZE) {
            double diff = e.getScreenY() - prevMouseResize.y;
            
            dimensions.height += diff;

            prevMouseResize.y = e.getScreenY();
        }
        else if (scene.getCursor() == Cursor.W_RESIZE) {
            double diff = e.getScreenX() - prevMouseResize.x;
            
            dimensions.width -= diff;
            origin.x += diff;

            prevMouseResize.x = e.getScreenX();
        }

        adjustWindow();
	}

	/**
     * Checks if the mouse is on one of the window boundaries as well as if the
     * mouse has been clicked while on the border. The cursor changes and begins
     * a resize if it is within 3 pixels of one of the borders.
     * 
     * @param e MouseEvent to track the location of the cursor
     */
	public static void checkResizable(MouseEvent e) {
        Scene scene = App.mainStage.getScene();

        if (e.getSceneY() <= 3) {
            scene.setCursor(Cursor.N_RESIZE);
        }
        else if (e.getSceneY() >= scene.getHeight() - 4) {
            scene.setCursor(Cursor.S_RESIZE);
        }
        else if (e.getSceneX() <= 3) {
            scene.setCursor(Cursor.W_RESIZE);
        }
        else if (e.getSceneX() >= scene.getWidth() - 4) {
            scene.setCursor(Cursor.E_RESIZE);
        }
        else {
            scene.setCursor(Cursor.DEFAULT);
        }
        
        // This will only fire when the mouse is clicked
        if (e.getButton() == MouseButton.PRIMARY) {
            prevMouseResize.x = e.getScreenX();
            prevMouseResize.y = e.getScreenY();
            
            // only set the resizing flag if on one of the borders (if on one of
            // the borders, the cursor will be non-default
            if (scene.getCursor() != Cursor.DEFAULT) {
                resizing = true;
            }
        }
        else {
            resizing = false;
        }
	}

    /**
     * Stops resizing the window, sets the cursor to default, and sets the 
     * resizing flag to false.
     */
	public static void stopResizing() {
        App.mainStage.getScene().setCursor(Cursor.DEFAULT);
        resizing = false;
	}

    /**
     * This is called at the end of every drag event
     */
    private static void adjustWindow() {
        App.mainStage.setX(origin.x);
        App.mainStage.setY(origin.y);
        App.mainStage.setWidth(dimensions.width);
        App.mainStage.setHeight(dimensions.height);
    }

    /**
     * This handles resizing the window to the side of the screen. If `side`
     * is LEFT, then it will occupy the left half of the screen, if `side` is
     * RIGHT, then it will occupy the right side of the screen.
     * 
     * @param side enum value representing which side of the screen to occupy
     */
    private static void snapToSide(SnapDirection side) {
        snappedToSide = true;
        Point corner = new Point();
        Dimensions size = new Dimensions();

        switch(side) {
			case LEFT:
                corner.x = 0;
                corner.y = 0;
                size.width = getWidth()/2;
                size.height = getHeight();
				break;
			case RIGHT:
                corner.x = getWidth()/2;
                corner.y = 0;
                size.width = getWidth()/2;
                size.height = getHeight();
				break;
        }

        App.mainStage.setX(corner.x);
        App.mainStage.setY(corner.y);
        App.mainStage.setWidth(size.width);
        App.mainStage.setHeight(size.height);
    }

    /**
     * Checks to see if the screen needs to be maximized (if the mouse is in the
     * upper boundary), or resized to fit half the screen (right side or left,
     * depending on where the mouse is).
     * 
     * @param e MouseEvent passed from drag()
     */
    private static void checkForExpansion(MouseEvent e) {
        
        if (e.getScreenY() <= 0) {
            toggleMaximized();
        }
        else if (e.getScreenX() <= 0) {
            snapToSide(SnapDirection.LEFT);
        }
        else if (e.getScreenX() >= getWidth() - 1) {
            snapToSide(SnapDirection.RIGHT);
        }
    }

    public static double getWidth() {
        return Screen.getPrimary().getVisualBounds().getWidth();
    }

    public static double getHeight() {
        return Screen.getPrimary().getVisualBounds().getHeight();
    }

    public static double getMinY() {
        return Screen.getPrimary().getVisualBounds().getMinY();
    }

    public static double getMinX() {
        return Screen.getPrimary().getVisualBounds().getMinX();
    }
}
