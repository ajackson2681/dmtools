
module dev.adamjackson.dmtools {
    requires transitive javafx.controls;
    requires transitive javafx.graphics;
	requires transitive java.sql;
    requires transitive javafx.fxml;
    requires transitive com.google.gson;
	requires com.github.oshi;
	requires javafx.base;
    requires javafx.web;
    
    opens dev.adamjackson.dmtools to javafx.fxml;
    exports dev.adamjackson.dmtools;
}
